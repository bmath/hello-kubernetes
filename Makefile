all: build docker-build docker-tag


foo: all kubernetes-deploy

deploy: docker-push kubernetes-deploy watch

build:
	go get
	go build

docker-build:
	docker build .

docker-run:
	echo FIXME

docker-tag:
	echo docker tag BUILD gcr.io/storageos-boozle/hello-kubernetes

docker-push:
	docker push gcr.io/storageos-boozle/hello-kubernetes

kubernetes-deploy:
	kubectl create -f deployment-manifest.yaml 
	kubectl create -f service-manifest.yaml

kubernetes-cleanup:
	kubectl delete service hello-kubernetes
	kubectl delete deployment hello-kubernetes

watch:
	watch 'kubectl get services; kubectl get pods'

perf-test:
	echo bzt -o scenarios.quick-test.requests=\"[\'http://SERVICE_ADDR/hello\']\"  perf-test.yaml

clean:
	for line in `cat .gitignore`;  do echo "$$line"; rm -rf $$line; done
