package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

func sayhello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello Kubernetes\n\ndf output is:\n\n")

	cmd := exec.Command("df", "-h")
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "%s\n", output)

	cmd = exec.Command("df", "-h", ".")
	output, err = cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "%s\n", output)
}

func main() {
	fmt.Printf("listening on 8888\n")
	http.HandleFunc("/hello", sayhello)
	err := http.ListenAndServe(":8888", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
