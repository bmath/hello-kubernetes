FROM golang:alpine
RUN mkdir /hello-kubernetes
ADD main.go /hello-kubernetes
WORKDIR /hello-kubernetes
RUN go build .

FROM alpine:latest
RUN adduser -S -D -H -h /app user
USER user
COPY --from=0 /hello-kubernetes/hello-kubernetes .
CMD ["./hello-kubernetes"]
